FROM debian:bullseye
RUN apt-get update && apt-get install -y \
    gcc \
    gfortran \
    perl \
    make \
    cmake \
    python3-distutils \
    python3-scipy \
    python3-six \
    bc \
    bzip2 \
    wget \
    ; \
    rm -rf /var/lib/apt/lists/*; \
    ln -s /usr/bin/python3 /usr/bin/python; \
    ln -s /usr/bin/pip3 /usr/bin/pip;
WORKDIR /tmp
RUN wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-3.13.4.tar.gz; \
    wget https://damask.mpie.de/pub/Download/Current/damask-2.0.3.tar.xz; \
    mkdir /tmp/petsc; \
    mkdir /opt/damask; \
    tar -xzf petsc-lite-3.13.4.tar.gz -C /tmp/petsc --strip 1; \
    tar -xJf damask-2.0.3.tar.xz -C /opt/damask --strip 1; \
    rm petsc-lite-3.13.4.tar.gz damask-2.0.3.tar.xz;
RUN cd /tmp/petsc; \
    ./configure --prefix=/opt/petsc \
                --download-fblaslapack \
                --download-fftw \
                --download-hdf5 \
                --download-openmpi \
                --with-cc=gcc \
                --with-cxx=g++ \
                --with-fc=gfortran; \
    make all; \
    make install; \
    cd /tmp; \
    rm -rf /tmp/petsc
ENV PETSC_DIR /opt/petsc
ENV DAMASK_DIR /opt/damask
WORKDIR $DAMASK_DIR
RUN sed -i 's/PETSC_VERSION_MINOR!=10/PETSC_VERSION_MINOR < 10/g' src/DAMASK_interface.f90; \
    source ./DAMASK_env.sh; \
    make spectral; \
    make processing

